FROM symphony-docker-local.jfrog.io/symphony/symphony-base:vmware-jre-8u141
MAINTAINER Oleg Tsvinev <oleg.tsvinev@gmail.com>

ENTRYPOINT ["/usr/lib/jre/bin/java", "-jar", "/usr/share/myservice/myservice.jar"]
#ENTRYPOINT ["/bin/bash"]


# Add Maven dependencies (not shaded into the artifact; Docker-cached)
# ADD target/lib           /usr/share/myservice/lib
# Add the service itself
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/myservice/myservice.jar
