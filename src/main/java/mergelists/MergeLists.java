package mergelists;

import java.util.Collections;
import java.util.List;

public class MergeLists {

    static public class Node {
        public int data;
        public Node next;

        public Node(int data, Node next) {
            this.data = data;
            this.next = next;
        }

    }

    static Node makeList(int... items) {
        Node head = null;
        for (int i = items.length - 1; i >= 0; i--) {
            head = new Node(items[i], head);
        }
        return head;
    }

    /**
     * Naturally in real code lists will be represented by one of the Java
     * collections implementing the java.util.List and use Collections.sort()
     * and care about efficiency later to avoid premature optimization
     * And if there's a problem, see implementations of
     * com.sun.javafx.collections.SortableList
     */
    static public List<Integer> realCodeMergeSortedLists(List<Integer> list1, List<Integer> list2) {
        if (null == list1) {
            return list2;
        }
        if (null == list2) {
            return list1;
        }
        list1.addAll(list2);
        Collections.sort(list1);
        return list1;
    }

    /**
     * Code that implements classic merge sort
     * for the sake of interview exercise
     * For real life code, see realCodeMergeSortedLists
     *
     * @param list1
     *            first list
     * @param list2
     *            second list
     * @return merged list
     */
    static public Node mergeSortedLists(Node list1, Node list2) {
        if (null == list1) {
            return list2;
        }
        if (null == list2) {
            return list1;
        }
        Node head = null;
        if (list1.data < list2.data) {
            head = list1;
        } else {
            head = list2;
            list2 = list1;
            list1 = head;
        }
        while (list1.next != null) {
            if (list1.next.data > list2.data) {
                Node n = list1.next;
                list1.next = list2;
                list2 = n;
            }
            list1 = list1.next;
        }
        if (list1.next == null) {
            list1.next = list2;
        }
        return head;
    }

}
