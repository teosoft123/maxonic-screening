package logfilesort;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * 3. Write a Java function that sorts a log file based on increasing timestamp
 * and decreasing severity...
 *
 * a. Without looking into performance:
 *
 * Implement a log parser that will create a LogEntry instance for each log
 * entry. Implement a comparator for log entries that compares timestamps and
 * severity level Read the log file into say SortedSet<LogEntry> Write file
 * element by element reading from SortedSet Any entry except timestamp and
 * severity is optional as a way to deal with improperly formatted lines,
 * because only timestamp and severity affect sorting.
 *
 * While it will work, the *space* performance will not optimal. For really big
 * file it will allocate significant amount of heap memory, i.e. one LogEntry
 * instance per line.
 *
 * Assuming log entry is limited to 1KB in size, 1TB file will have maximum 1GB
 * entries (1TB/1024) and the resulting structure will not fit into memory.
 *
 * Better algorithm will be a Hadoop map-reduce or use Apache Spark
 * for splitting the data into multiple subsets, sort them separately and
 * finally perform a merge on the resulting sorted subsets.
 *
 * Here though I'll implement it with Java8 streams and Comparator that compares
 * Strings but only parts we want, without creating a Java object for each string.
 * This "processor" can be used to sort parts of the files.
 * I leave the merge part out for now.
 *
 * Also, types/objects organization can be better but given the limited time, this will meet the requirements
 *
 */
public class LogFileSort {

    // log file date/time formatting is not of any ISO standard, hence the custom format
    public static DateTimeFormatter LOG_ENTRY_DATETIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSxxxx");

    // pattern used for both filtering malformed entries out and comparing entries
    // five groups of anything enclosed in brackets optionally separated by whitespace
    public static final Pattern LOG_ENTRY_PATTERN = Pattern.compile("^\\[(.*)\\]\\s*\\[(.*)\\]\\s*\\[(.*)\\]\\s*\\[(.*)\\]\\s*\\[(.*)\\]\\s*$");

    /**
     * this Comparator must only be used after malformed entries are filtered out
     */
    public static Comparator<String> logEntryComparator = new Comparator<String>() {
        @Override
        public int compare(String e1, String e2) {
            Matcher me1 = LOG_ENTRY_PATTERN.matcher(e1);
            Matcher me2 = LOG_ENTRY_PATTERN.matcher(e2);
            me1.matches();
            me2.matches();
            int severityOrder = String.CASE_INSENSITIVE_ORDER.compare(getSeverity(me1), getSeverity(me1));
            if(severityOrder != 0) {
                return severityOrder;
            }
            else {
                return getZonedDateTime(me1).compareTo(getZonedDateTime(me2));
            }
        }

        private String getSeverity(Matcher m) {
            return m.group(3);
        }

        private ZonedDateTime getZonedDateTime(Matcher m) {
             return ZonedDateTime.parse(m.group(1), LogFileSort.LOG_ENTRY_DATETIME_FORMAT);
        }
    };

    public static void logFileSort(Path src, Path dst) {
        try (Stream<String> source = Files.lines(src)) {
            source
                .filter(s -> {
                    return isValidLogEntry(s);
                })
                .sorted((s1, s2) -> {
                    return logEntryComparator.compare(s1, s2);
                })
                /**
                 * for now, printing them to stdout
                 * because I still need to do #4 :)
                 */
                .forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isValidLogEntry(String s) {
        Matcher m = LogFileSort.LOG_ENTRY_PATTERN.matcher(s);
        if(m.matches() && m.groupCount() == 5) {
            try {
                ZonedDateTime.parse(m.group(1), LogFileSort.LOG_ENTRY_DATETIME_FORMAT);
            }
            catch(DateTimeParseException e) {
                return false;
            }
        }
        return true;
    }

}
