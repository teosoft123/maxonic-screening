package stringsort;

import java.util.Arrays;
import java.util.stream.Collectors;

public class StringSort {

    public static String stringSort(String s, String separator) {
        if (null == s || s.isEmpty()) {
            return s;
        }
        if(null == separator || separator.isEmpty()) {
            throw new IllegalArgumentException("Separator cannot be null or empty");
        }
        String[] words = s.split(separator);
        Arrays.sort(words);
        String result = Arrays.stream(words).collect(Collectors.joining(separator));
        return result;
    }
}
