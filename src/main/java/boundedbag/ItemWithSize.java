package boundedbag;

public interface ItemWithSize {
    long getSizeBytes();
}
