package boundedbag;

/**
 * Implements bag-of-items collection<br>
 * Safe to use by multiple threads concurrently.
 *
 * <pre>
 * The total allowed size in bytes of all items contained in the bag is bounded by a constructor parameter.
 * Any offers that would cause the bag to grow beyond the allowed capacity (in bytes) should be refused and return false.
 * Take can return any item in the bag and will remove it from the bag, freeing up capacity.
 * </pre>
 *
 * <pre>
 * Implementation notes:
 * <ul><li>Requirement does not specify any order of stored elements. Implementing using a simple linked list</li>
 * <li>Better performance could be achieved by increased parallelism using multiple storage structures so that
 * multiple thread would have only one contention point, which is the current capacity read and write operations.
 * Namely if there's more storage buckets than there's threads all item offer/take operations can go in parallel
 * except for the current capacity operations.
 *
 * </li>
 * </ul>
 * </pre>
 *
 * @author oleg
 *
 * @param <T>
 */
public class BoundedConcurrentBag<T extends ItemWithSize> {

    private static class Node<T> {
        public Node(T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }

        private final T item;
        private final Node<T> next;
    }

    private final Object lock = new Object(); // can just as well synchronize on
                                              // "this"
    private final long capacity;
    private long currentCapacity = 0;
    private Node<T> head = null;

    public BoundedConcurrentBag(long capacity) {
        this.capacity = capacity;
    }

    /**
     * Offers item to be added to bag. If adding the item would exceed capacity,
     * item is not added
     *
     * @param item
     * @return true if item added, false if rejected
     */
    public boolean offer(T item) {
        if (null == item) {
            return false;
        }
        synchronized (lock) {
            final long newCurrentCapacity = currentCapacity + item.getSizeBytes();
            if (newCurrentCapacity > capacity) {
                return false;
            } else {
                head = new Node<T>(item, head);
                currentCapacity = newCurrentCapacity;
            }
            return true;
        }
    }

    /**
     * Returns any item from the bag, removing the item from the bag and
     * increasing capacity by the removed item size
     *
     * @return item or null if the bag is empty
     */
    public T take() {
        T item = null;
        /**
         * we have a bug here, but before fixing it, it'd be nice to write a test that catches it.
         * TODO write test to catch racing conditions
         */
        if (null != head) {
            synchronized (lock) {
                currentCapacity -= head.item.getSizeBytes();
                if (currentCapacity < 0) {
                    throw new IllegalStateException("If this happens, we have a bug");
                }
                item = head.item;
                head = head.next;
            }
        }
        return item;
    }

}
