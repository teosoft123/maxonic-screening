package mergelists;

import static org.testng.Assert.fail;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import mergelists.MergeLists.Node;

public class MergeListsTest {

    @Test(dataProvider = "positiveTests")
    public void testMergeSortedLists(Node list1, Node list2, Node expected) {
        assertOrdered(list1);
        assertOrdered(list2);
        Node mergedSortedList = MergeLists.mergeSortedLists(list1, list2);
        assertOrdered(mergedSortedList);
        assertListEquals(mergedSortedList, expected);
    }

    @DataProvider
    public Object[][] positiveTests() {
        return new Object[][] {
            new Object[] { makeList(1, 2, 3), makeList(1, 2, 3), makeList(1, 1, 2, 2, 3, 3) },
                         { makeList(1, 2), makeList(1, 3, 5), makeList(1, 1, 2, 3, 5) },
                         { makeList(1, 2), null, makeList(1, 2) },
                         { null, makeList(1, 2), makeList(1, 2) },
        };
    }

    static Node makeList(int... items) {
        return MergeLists.makeList(items);
    }

    /**
     * presumes ascending order
     *
     * @param list
     */
    public void assertOrdered(Node list) {
        int current = Integer.MIN_VALUE;
        for (Node n = list; n != null; n = n.next) {
            if (n.data > current) {
                current = n.data;
                continue;
            }
            if (n.data < current) {
                fail("List is not sorted in ascending order");
            }
        }
    }

    private void assertListEquals(Node actual, Node expected) {
        if(null == actual && null != expected || null != actual && null == expected) {
            fail("Lists are not equal");
        }
        for (Node l1 = actual, l2 = expected; l1 != null && l2 != null; l1 = l1.next, l2 = l2.next) {
            Assert.assertEquals(l1.data, l2.data);
        }

    }

}
