package logfilesort;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.regex.Matcher;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LogFileSortTest {

    @Test(dataProvider = "logEntries", enabled = false)
    public void testLogEntry(String logEntry) {
        Matcher m = LogFileSort.LOG_ENTRY_PATTERN.matcher(logEntry);
        Assert.assertTrue(m.matches(), "Entry does not math pattern");
        Assert.assertEquals(m.groupCount(), 5, "Malformed entry");
        ZonedDateTime.parse(m.group(1), LogFileSort.LOG_ENTRY_DATETIME_FORMAT);
    }

    @Test(dataProvider = "logFiles", enabled = false)
    public void testLogFileSort(Path source) {
        LogFileSort.logFileSort(source, Paths.get("/tmp/out.log"));
    }

    @DataProvider
    public Object[][] logFiles() {
        try {
            return new Object[][] {
                    new Object[] { Paths.get(LogFileSortTest.class.getResource("test-log-1.log").toURI()) }, };
        } catch (URISyntaxException e) {
            Assert.fail("Invalid file specification");
        }
        return null;
    }

    @DataProvider
    public Object[][] logEntries() {
        return new Object[][] {
                   new Object[] { "[2015-11-19 10:33:54.934+0000] [host1] [info] [class1] [message1 something]" },
                   new Object[] { "[2015-11-19 10:31:55.128+0000] [HOST2] [ERROR] [CLASS2] [MESSAGE2 random]" },
                   new Object[] { "[2015-11-19 10:31:55.128+0000] [HOST3] [INFO] [CLASS6] [MESSAGE5 from another host]" },
                   new Object[] { "[2015-11-19 10:37:55.246+0000] [HOST2] [WARN] [CLASS9] [MESSAGE9 again]" },
                   new Object[] { "[2015-11-19 10:35:55.267+0000] [HOST4] [INFO] [CLASS4] [MESSAGE4 too]" },
                   new Object[] { "[2015-11-19 10:22:55.307+0000] [HOST1] [ERROR] [CLASS5] [some error happened]" },
                   new Object[] { "[2015-11-19 10:18:55.377+0000] [HOST5] [INFO] [CLASS1] [MESSAGE1 status message]" },
                   new Object[] { "[2015-11-19 10:31:55.128+0000] [HOST7] [WARN] [CLASS3] [MESSAGE7 something fishy]" },
                   new Object[] { "[2015-11-19 10:43:55.667+0000] [HOST9] [INFO] [CLASS7] [MESSAGE8 normal message]" },
                   new Object[] { "[2015-11-19 10:32:55.782+0000] [HOST1] [ERROR] [CLASS1] [MESSAGE3 another error]" },
               };
    }

}
