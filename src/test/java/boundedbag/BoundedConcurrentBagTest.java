package boundedbag;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BoundedConcurrentBagTest {

    // totally fake class that take arbitrary size in constructor
    // items though are real for testing purposes
    // item size in bytes has nothing to do with real item size ;)
    public static class IntegerWithSize implements ItemWithSize {

        public IntegerWithSize(Integer item) {
            this.item = item;
            size = item;
        }

        private final Integer item;
        private final long size;

        @Override
        public long getSizeBytes() {
            return size;
        }

    }

    // @Test(dataProvider = "positive")
    @Test
    public void testBagFunctionality() {
        BoundedConcurrentBag<IntegerWithSize> bag = new BoundedConcurrentBag<>(3);
        Assert.assertTrue(bag.offer(new IntegerWithSize(1)));
        Assert.assertTrue(bag.offer(new IntegerWithSize(2)));
        Assert.assertFalse(bag.offer(new IntegerWithSize(3)));
        // here we're cheating, knowing that our storage is fifo - not really a black box test
        Assert.assertEquals(bag.take().item.intValue(), 2);
        Assert.assertEquals(bag.take().item.intValue(), 1);
        Assert.assertEquals(bag.take(), null);

    }

}
