package stringsort;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class StringSortTest {

    @Test(dataProvider = "positiveTests")
    public void testStringSortPositive(String input, String separator, String expected) {
        Assert.assertEquals(StringSort.stringSort(input, separator), expected);
    }

    @Test(dataProvider = "negativeTests", expectedExceptions = {IllegalArgumentException.class})
    public void testStringSortNegative(String input, String separator, String expected) {
        Assert.assertEquals(StringSort.stringSort(input, separator), expected);
    }

    @DataProvider
    public Object[][] positiveTests() {
        return new Object[][] {
            new Object[] { null, null, null },
            new Object[] { null, " ", null },
            new Object[] { "premature optimization is the root of all evil", " ",
                           "all evil is of optimization premature root the" },
            new Object[] { "good, deal, of, data", ", ", "data, deal, good, of" },
        };

    }

    @DataProvider
    public Object[][] negativeTests() {
        return new Object[][] {
            new Object[] { "sort", null, "sort" },
            new Object[] { "sort", "", "sort" },
        };

    }

}
